all: prog1 prog2 prog3 prog4 prog5

prog1: prog1.o
	ld -m elf_i386 -o $@ $<

prog1.o: prog1.asm
	nasm -f elf -g -o $@ $<

prog2: prog2.o
	ld -m elf_i386 -o $@ $<

prog2.o: prog2.asm
	nasm -f elf -g -o $@ $<

prog3: prog3.o
	ld -m elf_i386 -o $@ $<

prog3.o: prog3.asm
	nasm -f elf -g -o $@ $<

prog4: prog4.o
	ld -m elf_i386 -o $@ $<

prog4.o: prog4.asm
	nasm -f elf -g -o $@ $<

prog5: prog5.o
	ld -m elf_i386 -o $@ $<

prog5.o: prog5.asm
	nasm -f elf -g -o $@ $<

.PHONY: clean
clean:
	rm -rf *.o prog[0-9]
