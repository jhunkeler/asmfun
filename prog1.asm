global _start

section .data
	MAXLOOP db 10

section .text

; while edx <= ecx
loop:
	cmp ecx, edx
	jle exit

	; ebx++
	add edx, 1
	
	; loop()
	jmp loop

exit:
	; exit value is in edx, move it to ebx
	mov ebx, edx
	
	; clear registers
	mov edx, 0
	mov ecx, 0

	; initate exit syscall
	; exit(ebx)
	mov eax, 1
	int 80h
	ret
	
_start:
	mov ecx, [MAXLOOP]
	jmp loop

