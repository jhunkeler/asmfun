section .data
one	db	5
ol	equ	$-one
two	db	5

section .text
global _start

exit:
	mov ebx, 0
	mov eax, 1
	int 80h

addme:
	push ebp
	mov ebp, esp
	push edx

	mov edx, [ebp+12]
	add edx, [ebp+8]
	mov eax, edx
	
	pop edx
	pop ebp
	ret

_start:
	mov ebp, esp
	sub esp, 8

	mov ebx, one
	mov dword [esp+4], ebx
	mov dword [esp], 5
	call addme

	mov esp, ebp
	pop ebp
	call exit	
