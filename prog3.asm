;playing with the stack

section .data
arg	db	255

section .text
global _start

uhh:
	; save stack
	push ebp
	; create local stack
	mov ebp, esp
	; create "local" variable(s) by subtracting the stack by X bytes
	sub ebp, 4

	; first argument is 3 dwords (4 bytes * 3) in...
	; copy first argument to eax
	mov eax, [ebp+12]
	
	; mov eax (first arg) into local stack variable
	mov [ebp-4], eax
	; clear eax
	mov eax, 0

	; mov local variable into ebx 
	mov ebx, [ebp-4]
	
	; reset stack pointer
	; aka, giveth back what we taketh away
	add ebp, 4

	; restore stack
	mov esp, ebp
	pop ebp
	ret

_start:
	; save stack
	push ebp
	mov ebp, esp

	; push value of 'arg' to the stack from ebx
	mov ebx, [arg]	
	push ebx
	; clear ebx
	mov ebx, 0

	call uhh
	
	; restore ebx (because I can.) should be value of 'arg'
	pop ebx

	; restore stack
	mov esp, ebp
	pop ebp

	; exit program, ebx is return value
	mov eax, 1
	int 80h
	ret
