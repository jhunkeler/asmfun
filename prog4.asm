; begin playing around with variables!
section .bss
	fart_count:		resb	4

section .data
	fart_max:		db		255

section .text
	global _start

dofarts:
	push ebp
	mov ebp, esp
	add ebp, 4

	; set the fart_count to zero
	mov DWORD [fart_count], 0

	; save eax to the stack because we're gonna use it
	push eax

	jmp dofarts.keepgoing

dofarts.stop:
	;clean up and return
	pop eax
	pop ebp
	ret

dofarts.keepgoing:
	; put fart_count into a register (i wish i could do mem,mem)
	mov eax, [fart_count]

	; increment the fart_count
	add dword[fart_count], 1

	; has fart_count reached fart_max?
	jge dofarts.check
	; else, stop... this is redundant...
	jmp dofarts.stop

dofarts.check:
	; is fart_count (eax) fart_max?
	cmp eax, [fart_max]
	; if so, stop
	jge dofarts.stop
	; else keep going
	jmp dofarts.keepgoing


_start:
	push ebp
	mov ebp, esp

	call dofarts
	mov ebx, [fart_count]
	add ebx, [fart_max]
	
	mov esp, ebp
	pop ebp

	mov eax, 1
	;mov ebx, 0
	int 80h
	ret
